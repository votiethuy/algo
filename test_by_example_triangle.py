from unittest import TestCase
from unittest import main as test_main

from triangle import determine_type_of_triangle
from triangle import SCALENE, ISOSECLES, EQUILATERAL, ERROR

class TriangleTest(TestCase):
    
    def test_is_scalene(self):
        self.assertEqual(SCALENE,determine_type_of_triangle(2,3,4))
    
    def test_is_isosecles_triangle(self):
        self.assertEqual(ISOSECLES,determine_type_of_triangle(3,3,4))
    
    def test_is_equilateral_triangle(self):
        self.assertEqual(EQUILATERAL,determine_type_of_triangle(5,5,5))
    
    def test_is_error_triangle(self):
        self.assertEqual(ERROR,determine_type_of_triangle(1,2,3))
    
    def test_side_lte_zero_error_triangle(self):
        self.assertEqual(ERROR,determine_type_of_triangle(0,-1,2))


if __name__ == '__main__':
    test_main()