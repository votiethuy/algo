from unittest import TestCase
from unittest import main as test_main

import threading
import time

from circularqueue import CircularQueue


class CircularQueueTest(TestCase):
    def setUp(self):
        self.circular_queue = CircularQueue(5)

    def test_enqueue(self):
        self.circular_queue.enqueue(1)
        item = self.circular_queue.dequeue()
        self.assertEqual(1, item)
    
    def test_dequeue(self):
        self.circular_queue.enqueue(1)
        self.circular_queue.enqueue(2)
        item = self.circular_queue.dequeue()
        self.assertEqual(1, item)
    
    def test_circular_queue(self):
        self.circular_queue.enqueue(1)
        self.circular_queue.enqueue(2)
        self.circular_queue.enqueue(3)
        self.circular_queue.enqueue(4)
        self.circular_queue.enqueue(5)
        self.circular_queue.enqueue(6)
        item = self.circular_queue.dequeue()
        self.assertEqual(2, item)
    
    def test_size_queue(self):
        self.circular_queue.enqueue(1)
        self.circular_queue.enqueue(2)
        self.assertEqual(2,self.circular_queue.size())
    
    def test_raise_error_dequeue_empty_queue(self):
        with self.assertRaises(IndexError):
            self.circular_queue.dequeue()
    
    def test_synchronized_enqueue(self):
        def enqueue():
            for i in range(0,10):
                self.circular_queue.enqueue(i)
                time.sleep(0.1)
        
        thread1 = threading.Thread(target=enqueue)
        thread2 = threading.Thread(target=enqueue)
        thread1.start()
        thread2.start()
        thread1.join()
        thread2.join()
        item = self.circular_queue.dequeue()
        self.assertEqual(7,item)
    
    def test_synchronized_dequeue(self):
        for i in range(0,5):
            self.circular_queue.enqueue(i)
        def dequeue():
            for i in range(0,2):
                self.circular_queue.dequeue()
                time.sleep(0.1)
        
        thread1 = threading.Thread(target=dequeue)
        thread2 = threading.Thread(target=dequeue)
        thread1.start()
        thread2.start()
        thread1.join()
        thread2.join()
        item = self.circular_queue.dequeue()
        self.assertEqual(4,item)

if __name__ == '__main__':
    test_main()