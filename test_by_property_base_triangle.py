from unittest import TestCase
from unittest import main as test_main

from hypothesis import given
from hypothesis.strategies import tuples, integers

from triangle import determine_type_of_triangle
from triangle import SCALENE, ISOSECLES, EQUILATERAL, ERROR

def is_triangle(a,b,c):
    return a + b > c and a + c > b and b + c > a

class HypothesisTriangleTest(TestCase):
    @given(tuples(integers(min_value=1),integers(min_value=1),integers(min_value=1)).filter(lambda x: 
        x[0] != x[1] and x[0] != x[2] and x[1] != x[2] and is_triangle(x[0],x[1],x[2])))
    def test_hypothesis_is_scalene(self, x):
        self.assertEqual(SCALENE,determine_type_of_triangle(x[0],x[1],x[2]))
    
    @given(tuples(integers(min_value=1),integers(min_value=1)).filter(lambda x: 
        x[0] != x[1] and is_triangle(x[0],x[0],x[1])))
    def test_hypothesis_is_isosecles_triangle(self, x):
        self.assertEqual(ISOSECLES,determine_type_of_triangle(x[0],x[0],x[1]))

    @given(integers(min_value=1).filter(lambda x: is_triangle(x,x,x)))
    def test_hypothesis_is_equilateral_triangle(self, x):
        self.assertEqual(EQUILATERAL,determine_type_of_triangle(x,x,x))

    @given(tuples(integers(min_value=1),integers(min_value=1),integers(min_value=1)).filter(lambda x: 
        x[0] + x[1] <= x[2] or x[1] + x[2] <= x[0] or x[0] + x[2] <= x[1]))
    def test_hypothesis_is_error_triangle(self, x):
        self.assertEqual(ERROR,determine_type_of_triangle(x[0],x[1],x[2]))
    
    @given(tuples(integers(),integers(),integers()).filter(lambda x: 
        x[0] <= 0 or x[1] <= 0 or x[2] <= 0))
    def test_hypothesis_side_lte_zero(self, x):
        self.assertEqual(ERROR,determine_type_of_triangle(x[0],x[1],x[2]))

if __name__ == '__main__':
    test_main()