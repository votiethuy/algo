SCALENE = 1
ISOSECLES = 2
EQUILATERAL = 3
ERROR = 4

def determine_type_of_triangle(a,b,c):
    """
    1=scalene, 2=isosceles, 3=equilateral, 4=error
    """
    if a <= 0 or b <= 0 or c <= 0:
        return ERROR
    if a + b <= c or b + c <= a or a + c <= b:
        return ERROR
    elif a == b and b == c and c == a:
        return EQUILATERAL
    elif a != b and b != c and a != c:
        return SCALENE
    elif a == b or b == c or c == a:
        return ISOSECLES