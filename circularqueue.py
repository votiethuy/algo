import threading

def synchronized_with_attr(lock_name):
    
    def decorator(method):
			
        def synced_method(self, *args, **kws):
            lock = getattr(self, lock_name)
            with lock:
                return method(self, *args, **kws)
                
        return synced_method
		
    return decorator

class CircularQueue:
    def __init__(self,max_size):
        self.lock = threading.RLock()
        self.items = [None for i in range(max_size)]
    
    @synchronized_with_attr("lock")
    def enqueue(self, item):
        self.items.pop(0)
        self.items.append(item)
    
    @synchronized_with_attr("lock")
    def dequeue(self):
        temp_items = [item for item in self.items if item is not None]
        self.items.pop(0)
        return temp_items[0]
    
    @synchronized_with_attr("lock")
    def size(self):
        return len([item for item in self.items if item is not None])
