# README #
## Prequisite ##
* Python 3.6.2
* hypothesis 3.24.1 (optional) for property base testing
* ipython==6.1.0 (optional)
## Using Docker instead ##
* docker-compose up --build
* docker ps -a # to get container id
* docker exec -it *container_id* bash

## Test ##

#### 1. Determine type of Triangle ####
> I've 2 test file: one for example base test, one for property base test. If you want to run propery base test, 
> you will need to install hypothesis.
>
>    python test_by_example_triangle.py
>
>    python test_by_property_base_triangle.py
> 	
> Reference for property base testing:
> https://hypothesis.readthedocs.io/en/latest/manifesto.html

#### 2. Circular Buffer ####
> I've implement by using List in python. Pop at head is not very good performance. We almost using dequeue datastructure, 
> but this is requirement, so i just implement by concept.
> Running test by command:
>
>    python test_circularqueue.py
>


